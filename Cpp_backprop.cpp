#include <iostream>

#include <chrono>
#include <cmath>
#include <fstream>
#include <sstream>

#include <random>


#include <utility>
#include <vector>
#include <string>
#include <tuple>

using namespace std;



constexpr int index(int row, int col, int internal) {
    return row + col*internal;
}

// Matrix Class
class Matrix {
private:
    double* data;
    int internal_numRows;
    int internal_numCols;


public:
    Matrix() : data(nullptr), internal_numRows(0), internal_numCols(0) {}

    Matrix(int rows, int cols) : data(new double[rows * cols]), internal_numRows(rows), internal_numCols(cols) {
        for (int i = 0; i < internal_numRows * internal_numCols; ++i)
            data[i] = 0.0;
    }

    Matrix(const Matrix& other) : data(new double[other.internal_numRows * other.internal_numCols]), internal_numRows(other.internal_numRows),
                                  internal_numCols(other.internal_numCols) {
        for (int i = 0; i < internal_numRows * internal_numCols; ++i)
            data[i] = other.data[i];
    }

    Matrix(Matrix&& other)  noexcept : data(other.data), internal_numRows(other.internal_numRows), internal_numCols(other.internal_numCols) {
        other.data = nullptr;
        other.internal_numRows = 0;
        other.internal_numCols = 0;
    }

    int numRows() const {
        return internal_numRows;
    }

    int numCols() const {
        return internal_numCols;
    }

    Matrix& operator = (const Matrix& other) {
        double* new_data = new double[other.internal_numRows * other.internal_numCols];
        for (int i = 0; i < other.internal_numRows * other.internal_numCols; ++i)
            new_data[i] = other.data[i];

        delete[] data;
        data = new_data;
        internal_numRows = other.internal_numRows;
        internal_numCols = other.internal_numCols;

        return *this;
    }

    Matrix& operator = (Matrix&& other)  noexcept {
        data = other.data;
        internal_numRows = other.internal_numRows;
        internal_numCols = other.internal_numCols;

        other.data = nullptr;
        other.internal_numRows = 0;
        other.internal_numCols = 0;

        return *this;
    }

    ~Matrix() {
        delete[] data;
    }

    double& operator()(int row, int col) {
        return data[index(row, col, internal_numRows)];
    }

    const double& operator()(int row, int col) const {
        return data[index(row, col, internal_numRows)];
    }


};

// matrix addition
Matrix operator+(const Matrix& lhs, const Matrix &rhs) {
    Matrix result(lhs.numRows(), lhs.numCols());

    // Broadcast
    if (rhs.numRows() == 1) {
        for (int i = 0; i < lhs.numRows(); ++i) {
            for (int j = 0; j < lhs.numCols(); ++j) {
                result(i, j) = lhs(i, j) + rhs(0, j);
            }
        }

        return result;
    }

    for (int i = 0; i < lhs.numRows(); ++i) {
        for (int j = 0; j < lhs.numCols(); ++j) {
            result(i, j) = lhs(i, j) + rhs(i, j);
        }
    }

    return result;
}

// matrix subtraction
Matrix operator-(const Matrix& lhs, const Matrix& rhs) {
    Matrix result(lhs.numRows(), lhs.numCols());

    for (int i = 0; i < lhs.numRows(); ++i) {
        for (int j = 0; j < lhs.numCols(); ++j) {
            result(i, j) = lhs(i, j) - rhs(i, j);
        }
    }
    return result;
}

// scalar addition
Matrix operator+(const double& lhs, const Matrix& rhs) {
    Matrix result(rhs.numRows(), rhs.numCols());

    for (int i = 0; i < rhs.numRows(); ++i) {
        for (int j = 0; j < rhs.numCols(); ++j) {
            result(i, j) = lhs + rhs(i, j);
        }
    }

    return result;
}

// scalar subtraction
Matrix operator-(const double& lhs, const Matrix& rhs) {
    Matrix result(rhs.numRows(), rhs.numCols());

    for (int i = 0; i < rhs.numRows(); ++i)
        for (int j = 0; j < rhs.numCols(); ++j)
            result(i,j) = lhs - rhs(i,j);

    return result;
}

// matrix multiplication (pointwise)
Matrix operator*(const Matrix& lhs, const Matrix& rhs) {
    Matrix result(lhs.numRows(), lhs.numCols());

    for (int i = 0; i < lhs.numRows(); ++i)
        for (int j = 0; j < lhs.numCols(); ++j)
            result(i,j) = lhs(i,j) * rhs(i,j);

    return result;
}

// Scalar multiplication of a matrix
Matrix operator*(const double& scalar, const Matrix& matrix) {
    Matrix result(matrix.numRows(), matrix.numCols());

    // Perform scalar multiplication for each element in the matrix
    for (int i = 0; i < matrix.numRows(); ++i) {
        for (int j = 0; j < matrix.numCols(); ++j) {
            result(i, j) = scalar * matrix(i, j);
        }
    }

    return result;
}

// Matrix multiplication
Matrix matmul(const Matrix& matrix1, const Matrix& matrix2, bool transpose_matrix1 = false, bool transpose_matrix2 = false) {
    const int m1 = matrix1.numRows();
    const int n1 = matrix1.numCols();
    const int m2 = matrix2.numRows();
    const int n2 = matrix2.numCols();

    // Check the transpose flags and matrix dimensions to determine the multiplication type
    if (!transpose_matrix1 && !transpose_matrix2) {
        // Regular matrix multiplication
        Matrix result(m1, n2);

        for (int i = 0; i < m1; ++i) {
            for (int j = 0; j < n2; ++j) {
                result(i, j) = 0;
                for (int k = 0; k < n1; ++k) {
                    result(i, j) += matrix1(i, k) * matrix2(k, j);
                }
            }
        }

        return result;
    }

    if (!transpose_matrix1) {
        // Matrix multiplication with transpose of the second matrix
        Matrix result(m1, m2);

        for (int i = 0; i < m1; ++i) {
            for (int j = 0; j < m2; ++j) {
                result(i, j) = 0;
                for (int k = 0; k < n1; ++k) {
                    result(i, j) += matrix1(i, k) * matrix2(j, k);
                }
            }
        }

        return result;
    }

    if (!transpose_matrix2) {
        // Matrix multiplication with transpose of the first matrix
        Matrix result(n1, n2);

        for (int i = 0; i < n1; ++i) {
            for (int j = 0; j < n2; ++j) {
                result(i, j) = 0;
                for (int k = 0; k < m1; ++k) {
                    result(i, j) += matrix1(k, i) * matrix2(k, j);
                }
            }
        }

        return result;
    }

    // Matrix multiplication with transposes of both matrices
    Matrix result(n1, m2);

    for (int i = 0; i < n1; ++i) {
        for (int j = 0; j < m2; ++j) {
            result(i, j) = 0;
            for (int k = 0; k < m1; ++k) {
                result(i, j) += matrix1(k, i) * matrix2(j, k);
            }
        }
    }

    return result;
}

// scalar division
Matrix operator / (const double& numerator, const Matrix& denominator) {
    Matrix result(denominator.numRows(), denominator.numCols());

    for (int i = 0; i < denominator.numRows(); ++i) {
        for (int j = 0; j < denominator.numCols(); ++j) {
            result(i, j) = numerator / denominator(i, j);
        }
    }

    return result;
}

double squared_sum(const Matrix& input) {
    double sum = 0;

    for (int i = 0; i < input.numRows(); ++i) {
        for (int j = 0; j < input.numCols(); ++j) {
            sum += input(i, j) * input(i, j);
        }
    }

    return sum;
}

Matrix sqrt(const Matrix& matrix) {
    Matrix result(matrix.numRows(), matrix.numCols());

    for (int i = 0; i < matrix.numRows(); ++i)
        for (int j = 0; j < matrix.numCols(); ++j)
            result(i, j) = sqrt(matrix(i, j));

    return result;
}

Matrix row_sum(const Matrix& matrix) {
    Matrix sum(1, matrix.numCols());

    for (int j = 0; j < matrix.numCols(); ++j) {
        for (int i = 0; i < matrix.numRows(); ++i) {
            sum(0, j) += matrix(i, j);
        }
    }

    return sum;
}

Matrix tanh(const Matrix& matrix) {
    Matrix result(matrix.numRows(), matrix.numCols());

    for (int i = 0; i < matrix.numRows(); ++i) {
        for (int j = 0; j < matrix.numCols(); ++j) {
            result(i, j) = tanh(matrix(i, j));
        }
    }

    return result;
}


// Xavier random initialization of weights
Matrix xavier_initializer(int input_dim, int output_dim) {
    // Calculate Xavier standard deviation
    double xavier_standard_deviation = sqrt(2.0 / (input_dim + output_dim));

    // Seed the random number generator
    int random_seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine random_generator(random_seed);

    // Create a normal distribution with mean 0 and Xavier standard deviation
    normal_distribution<double> distribution(0.0, xavier_standard_deviation);

    // Create a matrix to hold the initialized weights
    Matrix initialized_weights(input_dim, output_dim);

    // Initialize the weights with random values from the distribution
    for (int i = 0; i < input_dim; ++i) {
        for (int j = 0; j < output_dim; ++j) {
            initialized_weights(i, j) = distribution(random_generator);
        }
    }

    return initialized_weights;
}

// Adam Optimizer - stochastic update

tuple<Matrix, Matrix, Matrix> adam_optimization(Matrix& weights, Matrix& gradient, Matrix& first_moment, Matrix& second_moment, double learning_rate, int iteration) {
    // Adam hyperparameters
    double beta1 = 0.9;
    double beta2 = 0.999;
    double epsilon = 1e-8;

    // Update the first moment estimate
    Matrix updated_first_moment = beta1 * first_moment + (1.0 - beta1) * gradient;

    // Update the second moment estimate
    Matrix updated_second_moment = beta2 * second_moment + (1.0 - beta2) * (gradient * gradient);

    // Bias correction for the first moment estimate
    Matrix first_moment_bias_corrected = (1.0 / (1.0 - pow(beta1, iteration))) * updated_first_moment;

    // Bias correction for the second moment estimate
    Matrix second_moment_bias_corrected = (1.0 / (1.0 - pow(beta2, iteration))) * updated_second_moment;

    // Compute the scaling factor
    Matrix scaling_factor = 1.0 / (epsilon + sqrt(second_moment_bias_corrected));

    // Update the weights using the Adam update rule
    weights = weights - (learning_rate * (first_moment_bias_corrected * scaling_factor));

    // Return the updated weights and moment estimates
    return make_tuple(weights, updated_first_moment, updated_second_moment);
}

// Load data from a file and return matrices X_matrix, Y_matrix, and the size num_data_points
tuple<Matrix, Matrix, int> small_data_loading(const string& file_name)
{
    // Open the file for reading
    ifstream input_file(file_name);
    if (!input_file)
    {
        cerr << "Could not open file: " << file_name << "\n";
    }

    vector<double> x_values;
    vector<double> y_values;
    string line;

    // Read data line by line from the file
    while (getline(input_file, line))
    {
        double x, y;
        istringstream line_stream(line);
        line_stream >> x >> y;
        x_values.push_back(x);
        y_values.push_back(y);
    }

    // Get the number of data points
    int num_data_points = x_values.size();

    // Create matrices X_matrix and Y_matrix
    Matrix X_matrix(num_data_points, 1);
    Matrix Y_matrix(num_data_points, 1);

    // Fill the matrices with data
    for (int i = 0; i < num_data_points; ++i)
    {
        X_matrix(i, 0) = x_values[i];
        Y_matrix(i, 0) = y_values[i];
    }

    // Return the matrices and size as a tuple
    return make_tuple(X_matrix, Y_matrix, num_data_points);
}

tuple<Matrix, Matrix, int> medium_data_loading (const string& file_name) {
    vector<vector<double>> x_inputs;
    vector<vector<double>> y_inputs;

    ifstream input_file(file_name);
    if (!input_file)
    {
        cerr << "Could not open file: " << file_name << "\n";
    }
    string line;
    while (getline(input_file, line)) {
        vector<double> point_input;
        vector<double> point_output;

        istringstream line_stream(line);
        for (int i = 0; i < 10; i++) {
            double x;
            line_stream >> x;

            point_input.push_back(x);
        }
        x_inputs.push_back(point_input);

        for (int i = 0; i < 2; i++) {
            double y;
            line_stream >> y;
            point_output.push_back(y);
        }
        y_inputs.push_back(point_output);

    }
    Matrix inputs (x_inputs.size(), 10);
    Matrix outputs (y_inputs.size(), 2);

    for (int i = 0; i < x_inputs.size(); i++) {
        for (int j = 0; j < x_inputs[i].size(); j++) {
            inputs(i, j) = x_inputs[i][j];
        }
    }

    for (int i = 0; i < y_inputs.size(); i++) {
        for (int j = 0; j < y_inputs[i].size(); j++) {
            inputs(i, j) = y_inputs[i][j];
        }
    }

    return make_tuple(inputs, outputs, x_inputs.size());
}

tuple<Matrix, Matrix, int> mnist_data_loading (const string& file_name) {
    vector<vector<double>> x_inputs;
    vector<vector<double>> y_inputs;

    ifstream input_file(file_name);
    if (!input_file)
    {
        cerr << "Could not open file: " << file_name << "\n";
    }
    string line;
    while (getline(input_file, line)) {
        vector<double> point_input;
        vector<double> point_output;

        istringstream line_stream(line);
        for (int i = 0; i < 28*28; i++) {
            double x;
            line_stream >> x;

            point_input.push_back(x);
        }
        x_inputs.push_back(point_input);

        for (int i = 0; i < 20; i++) {
            double y;
            line_stream >> y;
            point_output.push_back(y);
        }
        y_inputs.push_back(point_output);

    }
    Matrix inputs (x_inputs.size(), 28*28);
    Matrix outputs (y_inputs.size(), 20);

    for (int i = 0; i < x_inputs.size(); i++) {
        for (int j = 0; j < x_inputs[i].size(); j++) {
            inputs(i, j) = x_inputs[i][j];
        }
    }

    for (int i = 0; i < y_inputs.size(); i++) {
        for (int j = 0; j < y_inputs[i].size(); j++) {
            inputs(i, j) = y_inputs[i][j];
        }
    }

    return make_tuple(inputs, outputs, x_inputs.size());
}

// Initialize Neural Network (weights & biases)
tuple<vector<Matrix>, vector<Matrix>> initialize_neural_network(const vector<int>& layer_sizes) {
    vector<Matrix> weight_matrices;
    vector<Matrix> bias_matrices;
    int num_layers = layer_sizes.size();
    for (int layer = 0; layer < num_layers - 1; ++layer) {
        // Initialize weight matrix using Xavier initialization
        Matrix weight_matrix = xavier_initializer(layer_sizes[layer], layer_sizes[layer + 1]);
        // Initialize bias matrix with appropriate dimensions
        Matrix bias_matrix({1, layer_sizes[layer + 1]});
        // Add the initialized weight and bias matrices to the corresponding vectors
        weight_matrices.push_back(weight_matrix);
        bias_matrices.push_back(bias_matrix);
    }
    return make_tuple(weight_matrices, bias_matrices);
}



using namespace std::chrono;

class NeuralNetwork {
private:
    Matrix Input; // Input data
    Matrix Output; // Output data

    vector<Matrix> weightMomentum; // Adam optimizer momentum for weights
    vector<Matrix> biasMomentum; // Adam optimizer momentum for biases
    vector<Matrix> weightVelocity; // Adam optimizer velocity for weights
    vector<Matrix> biasVelocity; // Adam optimizer velocity for biases

    vector<int> layerSizes; // Sizes of each layer in the neural network
    int totalLayers; // Total number of layers

    vector<Matrix> weights; // Neural network weights
    vector<Matrix> biases; // Neural network biases

    vector<Matrix> hiddenUnits; // Hidden units (output of each layer)
    vector<Matrix> activatedUnits; // Activated units (output after activation function)

    vector<Matrix> gradients; // Backpropagation gradients
    vector<Matrix> lossWeights; // Gradients of the loss with respect to weights
    vector<Matrix> lossBiases; // Gradients of the loss with respect to biases



public:
    // Constructor
    NeuralNetwork(Matrix  inputData, Matrix  outputData, const vector<int>& _layerSizes) : Input(std::move(inputData)), Output(std::move(outputData)), layerSizes(_layerSizes)
    {
        int numDataPoints = Input.numRows();
        totalLayers = layerSizes.size();

        // Initialize weights and biases
        auto initializationResult = initialize_neural_network(layerSizes);
        weights = get<0>(initializationResult);
        biases = get<1>(initializationResult);

        // Initialize hidden units
        for (int layer = 0; layer < totalLayers; ++layer) {
            Matrix hiddenUnitsData(numDataPoints, layerSizes[layer]);
            Matrix activatedUnitsData(numDataPoints, layerSizes[layer]);
            hiddenUnits.push_back(hiddenUnitsData);
            activatedUnits.push_back(activatedUnitsData);
        }

        // Initialize backpropagation gradients
        for (int layer = 0; layer < totalLayers - 1; ++layer) {
            Matrix gradientsData(numDataPoints, layerSizes[layer + 1]);
            Matrix lossWeightsData(layerSizes[layer], layerSizes[layer + 1]);
            Matrix lossBiasesData(1, layerSizes[layer + 1]);
            gradients.push_back(gradientsData);
            lossWeights.push_back(lossWeightsData);
            lossBiases.push_back(lossBiasesData);
        }

        // Initialize Adam optimizer parameters
        for (int layer = 0; layer < totalLayers - 1; ++layer) {
            Matrix mtWeightsData(layerSizes[layer], layerSizes[layer + 1]);
            Matrix mtBiasesData(1, layerSizes[layer + 1]);
            Matrix vtWeightsData(layerSizes[layer], layerSizes[layer + 1]);
            Matrix vtBiasesData(1, layerSizes[layer + 1]);
            weightMomentum.push_back(mtWeightsData);
            biasMomentum.push_back(mtBiasesData);
            weightVelocity.push_back(vtWeightsData);
            biasVelocity.push_back(vtBiasesData);
        }

    }

    // Loss Function
    double lossFunction() {
        // Feed forward
        hiddenUnits[0] = Input;
        activatedUnits[0] = Input;

        for (int layer = 0; layer < totalLayers - 1; ++layer) {
            hiddenUnits[layer + 1] = matmul(activatedUnits[layer], weights[layer]) + biases[layer];
            activatedUnits[layer + 1] = tanh(hiddenUnits[layer + 1]);
        }

        double loss = 0.5 * squared_sum(hiddenUnits[totalLayers - 1] - Output);

        // Backpropagation
        gradients[totalLayers - 2] = hiddenUnits[totalLayers - 1] - Output;

        for (int layer = totalLayers - 2; layer > 0; --layer) {
            lossWeights[layer] = matmul(hiddenUnits[layer], gradients[layer], true, false);
            lossBiases[layer] = row_sum(gradients[layer]);

            gradients[layer - 1] = (1.0 - activatedUnits[layer] * activatedUnits[layer]) * matmul(gradients[layer], weights[layer], false, true);
        }

        lossWeights[0] = matmul(hiddenUnits[0], gradients[0], true, false);
        lossBiases[0] = row_sum(gradients[0]);

        return loss;
    }


    // Training method
    void train(int maxIterations, double learningRate) {
        chrono::duration<double> elapsedTime{};
        double loss;

        auto startTime = high_resolution_clock::now();

        cout << "starting\n";

        for (int iteration = 1; iteration < maxIterations + 1; ++iteration) {
            // Compute loss and gradients
            loss = lossFunction();

            // Update parameters using Adam optimizer
            for (int layer = 0; layer < totalLayers - 1; ++layer) {
                auto adamUpdateWeights = adam_optimization(weights[layer], lossWeights[layer], weightMomentum[layer],
                                                           weightVelocity[layer], learningRate, iteration);
                weights[layer] = get<0>(adamUpdateWeights);
                weightMomentum[layer] = get<1>(adamUpdateWeights);
                weightVelocity[layer] = get<2>(adamUpdateWeights);


                auto adamUpdateBiases = adam_optimization(biases[layer], lossBiases[layer], biasMomentum[layer],
                                                          biasVelocity[layer], learningRate, iteration);
                biases[layer] = get<0>(adamUpdateBiases);
                biasMomentum[layer] = get<1>(adamUpdateBiases);
                biasVelocity[layer] = get<2>(adamUpdateBiases);

                if (iteration % 10 == 0)
                {
                    auto finish_time = high_resolution_clock::now();
                    auto elapsed = finish_time-startTime;
                    cout << "iteration: " << iteration
                         << ", loss: " << loss
                         << ", time: " << elapsed.count()
                         << ", learning rate: " << learningRate << "\n";
                    startTime = high_resolution_clock::now();
                }
            }
        }
    }

    // Prediction
    Matrix test(Matrix& input) {
        int numLayers = layerSizes.size();

        Matrix hidden = input;
        Matrix activated = input;

        for (int layer = 0; layer < numLayers - 1; ++layer) {
            hidden = matmul(activated, weights[layer]) + biases[layer];
            activated = tanh(hidden);
        }

        return hidden;
    }

};

int main() {

    ofstream outputFile;
    // Load training data
    tuple<Matrix, Matrix, int> trainingData = medium_data_loading("./medium-data.csv");
    Matrix X_train = get<0>(trainingData);
    Matrix Y_train = get<1>(trainingData);
    int N_train = X_train.numRows();
    cout << N_train << endl;


    // Load test data
    tuple<Matrix, Matrix, int> testData = medium_data_loading("./medium-data.csv");
    Matrix X_test = get<0>(testData);
    Matrix Y_test = get<1>(testData);
    int N_test = X_test.numRows();

    outputFile.open("output.csv");

    cout << "hello" << endl;

    // vector<vector<int>> layerSizes = {{28*28, 10}, {28*28, 10, 10}};//, {1, 10, 10, 1}, {1, 10, 10, 10, 1}, {1, 10, 10, 10, 10, 1}, {1, 10, 10, 10, 10, 10, 1}, {1, 10, 10, 10, 10, 10, 10, 1}, {1, 10, 10, 10, 10, 10, 10, 10, 1}, {1, 10, 10, 10, 10, 10, 10, 10, 10, 1}, {1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 1}, {1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 1}};
    vector<vector<int>> layerSizes = {{10, 2}, {10, 10, 2}, {10, 10, 10, 2}, {10, 10, 10, 10, 2}, {10, 10, 10, 10, 10, 2}, {10, 10, 10, 10, 10, 10, 2}};
    for (int i = 0; i < layerSizes.size(); ++i) {
        NeuralNetwork model(X_train, Y_train, layerSizes[i]);
        auto startTime = high_resolution_clock::now();
        model.train(50, 1e-3);
        model.train(50, 1e-4);
        model.train(50, 1e-5);
        model.train(50, 1e-6);
        auto finishTime = high_resolution_clock::now();

        cout << "finishTime" << endl;

        outputFile << i << "," << duration_cast<chrono::seconds>(finishTime - startTime).count();

        Matrix Y_pred = model.test(X_test);

        // Calculate relative RMS error
        double relativeError = sqrt(squared_sum(Y_test - Y_pred) / squared_sum(Y_test));

        // Print the relative RMS error
        cout << "RMS error: " << relativeError << "\n";

        outputFile << "," << relativeError << "\n";

    }

    return 0;
}
