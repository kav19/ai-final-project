import numpy as np
# import matplotlib.pyplot as plt
import time


def adam_optimization(weights, gradient_weights, moving_avg_grad_weights,
                           moving_avg_sq_grad_weights, learning_rate, iteration):
    # Adam hyperparameters. Values that are known to work well with more datasets
    beta1 = 0.9
    beta2 = 0.999
    epsilon = 1e-8

    # Update moving averages of gradients and squared gradients
    moving_avg_grad_weights = beta1 * moving_avg_grad_weights + (1 - beta1) * gradient_weights
    moving_avg_sq_grad_weights = beta2 * moving_avg_sq_grad_weights + (1 - beta2) * gradient_weights ** 2

    # Compute bias-corrected estimates of gradients and squared gradients
    bias_corrected_grad_weights = moving_avg_grad_weights / (1 - beta1 ** iteration)
    bias_corrected_sq_grad_weights = moving_avg_sq_grad_weights / (1 - beta2 ** iteration)

    # Compute scaling factor for gradients
    scaling_factor = 1 / (np.sqrt(bias_corrected_sq_grad_weights) + epsilon)

    # Update weights
    weights = weights - learning_rate * bias_corrected_grad_weights * scaling_factor

    return weights, moving_avg_grad_weights, moving_avg_sq_grad_weights


def xavier_init(size):
    # Compute input and output dimensions
    input_dim = size[0]
    output_dim = size[1]

    # Compute standard deviation for Xavier initialization
    xavier_stddev = np.sqrt(2 / (input_dim + output_dim))

    # Sample random values from a normal distribution with mean 0 and standard deviation xavier_stddev
    random_values = np.random.normal(loc=0.0, scale=xavier_stddev, size=(input_dim, output_dim))

    return random_values


def initialize_NN(layer_dimensions):
    # Initialize empty lists to hold weight and bias matrices
    weight_matrices = []
    bias_matrices = []

    # Determine the number of layers in the neural network
    num_layers = len(layer_dimensions)

    # Loop over each pair of adjacent layers in the network
    for l in range(0, num_layers - 1):
        # Initialize a weight matrix using Xavier initialization
        input_dim = layer_dimensions[l]
        output_dim = layer_dimensions[l + 1]
        weight_matrix = xavier_init(size=[input_dim, output_dim])

        # Initialize a bias matrix with zeros
        bias_matrix = np.zeros([1, output_dim])

        # Append the weight and bias matrices to their respective lists
        weight_matrices.append(weight_matrix)
        bias_matrices.append(bias_matrix)

    # Return the list of weight matrices and bias matrices
    return weight_matrices, bias_matrices


class NN:
    # Initialize the class
    def __init__(self, X, Y, layers):

        # data
        self.X = X  # N x P
        self.Y = Y  # N x Q

        # layers
        self.layers = layers  # P ----> Q

        # initialize NN
        self.weights, self.biases = initialize_NN(layers)

    def calculate_loss(self):
        # initialization
        num_layers = len(self.layers)
        weights = self.weights
        biases = self.biases

        # Store intermediate hidden layer activations and outputs
        hidden_layer_outputs = [None] * (num_layers - 1)
        hidden_layer_activations = [None] * (num_layers - 1)

        hidden_layer_outputs[0] = self.X
        hidden_layer_activations[0] = self.X

        # Forward propagation / step function
        for layer in range(0, num_layers - 2):
            hidden_layer_outputs[layer + 1] = np.dot(hidden_layer_activations[layer], weights[layer]) + biases[layer]
            hidden_layer_activations[layer + 1] = np.tanh(hidden_layer_outputs[layer + 1])

        # Output layer
        prediction = np.dot(hidden_layer_activations[num_layers - 2], weights[num_layers - 2]) + biases[num_layers - 2]

        # Compute loss
        loss = 0.5 * np.sum((prediction - self.Y) ** 2)

        # Backpropagation
        gradients = [None] * (num_layers - 1)
        loss_weights = [None] * (num_layers - 1)
        loss_biases = [None] * (num_layers - 1)

        gradients[num_layers - 2] = prediction - self.Y

        # iterate over hidden layers in reverse order
        for layer in range(num_layers - 2, 0, -1):
            # calculate gradients for the current layer weights
            # by multiplying the transpose of the previous hidden layer outputs
            # with the gradients of the current layer activations
            loss_weights[layer] = np.dot(hidden_layer_outputs[layer].T, gradients[layer])

            # calculate biases for the current layer by summing the gradients of the current layer activations
            loss_biases[layer] = np.sum(gradients[layer], axis=0)

            # backpropagate the gradients to the previous layer
            gradients[layer - 1] = (1 - hidden_layer_activations[layer] ** 2) * np.dot(gradients[layer], weights[layer].T)

        # calculate gradients for the first hidden layer
        loss_weights[0] = np.dot(hidden_layer_outputs[0].T, gradients[0])

        # calculate biases for the first hidden layer
        loss_biases[0] = np.sum(gradients[0], axis=0)

        return loss, loss_weights, loss_biases, prediction

    def train(self, num_iterations, learning_rate):

        # Get the number of layers in the neural network
        num_layers = len(self.layers)

        # Initialize Adam optimizer parameters for weights and biases
        weights_momentums, biases_momentums = [None] * (num_layers - 1), [None] * (num_layers - 1)
        weights_velocities, biases_velocities = [None] * (num_layers - 1), [None] * (num_layers - 1)

        # Initialize momentums and velocities for weights and biases
        for layer_index in range(0, num_layers - 1):
            weights_momentums[layer_index], biases_momentums[layer_index] = [np.zeros(self.weights[layer_index].shape),
                                                                             np.zeros(self.biases[layer_index].shape)]
            weights_velocities[layer_index], biases_velocities[layer_index] = [
                np.zeros(self.weights[layer_index].shape),
                np.zeros(self.biases[layer_index].shape)]

        # Initialize start time
        start_time = time.time()

        # Train the neural network for num_iterations iterations
        for iteration_index in range(1, num_iterations + 1):

            # Calculate loss, gradients, and predicted output
            loss, weights_gradients, biases_gradients, predicted_output = self.calculate_loss()

            # Update weights and biases using Adam optimizer
            for layer_index in range(0, num_layers - 1):
                # Update weights
                [self.weights[layer_index],
                 weights_momentums[layer_index],
                 weights_velocities[layer_index]] = adam_optimization(self.weights[layer_index],
                                                                      weights_gradients[layer_index],
                                                                      weights_momentums[layer_index],
                                                                      weights_velocities[layer_index],
                                                                      learning_rate, iteration_index)

                # Update biases
                [self.biases[layer_index],
                 biases_momentums[layer_index],
                 biases_velocities[layer_index]] = adam_optimization(self.biases[layer_index],
                                                                     biases_gradients[layer_index],
                                                                     biases_momentums[layer_index],
                                                                     biases_velocities[layer_index],
                                                                     learning_rate, iteration_index)

            # Print loss and elapsed time every 10 iterations
            if iteration_index % 10 == 0:
                elapsed_time = time.time() - start_time

                print("Num Adam Iterations = ", iteration_index, " calculated loss = ", loss)

                start_time = time.time()

    def test(self, X):
        num_layers = len(self.layers)
        weights = self.weights
        biases = self.biases

        # Initialize input and hidden layers
        input_layer = X
        hidden_layer = X

        # Iterate through all but the last two layers
        for layer in range(0, num_layers - 2):
            # Compute the weighted sum of the input layer and biases
            weighted_sum = np.dot(hidden_layer, weights[layer]) + biases[layer]
            # Apply the tanh activation function to the weighted sum to compute the hidden layer
            hidden_layer = np.tanh(weighted_sum)

        # Compute the output layer by taking the weighted sum of the hidden layer and biases of the second to last layer
        output_layer = np.dot(hidden_layer, weights[num_layers - 2]) + biases[num_layers - 2]

        # Return the predicted output
        return output_layer


if __name__ == "__main__":
    # Load the training data
    training_data = np.loadtxt("training_data.csv")

    # Split the training data into input and output arrays
    training_input = training_data[:, 0:1]
    training_output = training_data[:, 1:2]

    # Create a new neural network model
    model = NN(training_input, training_output, layers=[1] + 2 * [10] + [1])

    # Train the model using different learning rates
    model.train(10000, 1e-3)
    model.train(10000, 1e-4)
    model.train(10000, 1e-5)
    model.train(10000, 1e-6)

    # Load the test data
    test_data = np.loadtxt("test_data.csv")

    # Split the test data into input and output arrays
    input = test_data[:, 0:1]
    output = test_data[:, 1:2]

    # Plot the training and test data
    #plt.figure()
    #plt.plot(training_input, training_output, 'kx')
    #plt.plot(input, output, 'b-')

    # Use the trained model to predict the output for the test data
    prediction = model.test(input)

    # Compute the error between the predicted and actual outputs
    error = np.linalg.norm(prediction - output) / np.linalg.norm(output)
    print('RMS Error %e ', error)

    # Plot the predicted output
    #plt.plot(input, prediction, 'r--')

    # Show the plot
    #plt.show()
