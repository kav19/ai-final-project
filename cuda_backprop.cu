#include <stdio.h>
#include <cuda_runtime.h>
#include <vector>
#include <chrono>
#include <random>

using dtype = float;

__global__ void matrix_mul(dtype *dst, dtype *A, dtype *B, size_t m, size_t n, size_t k){
    size_t row = blockIdx.y * blockDim.y + threadIdx.y; 
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;
    dtype sum = 0;
    if( col < k && row < m){
        for(size_t i = 0; i < n; i++)
            sum += A[row * n + i] * B[i * k + col];
        dst[row * k + col] = sum;
    }
}

__global__ void matrix_add(dtype*dst, dtype *A, dtype *B, size_t w){
    size_t row = blockIdx.y * blockDim.y + threadIdx.y; 
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;
    size_t idx = row*w + col;
    dst[idx] = A[idx] + B[idx];
}

__global__ void matrix_sub(dtype*dst, dtype *A, dtype *B, size_t w){
    size_t row = blockIdx.y * blockDim.y + threadIdx.y; 
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;
    size_t idx = row*w + col;
    dst[idx] = A[idx] - B[idx];
}

__global__ void matrix_zero(dtype *A, size_t w, size_t h){
    size_t row = blockIdx.y * blockDim.y + threadIdx.y; 
    size_t col = blockIdx.x * blockDim.x + threadIdx.x;
    A[row*w + col] = 0;
}

// __global__ void matrix_dense(dtype *dst, dtype* A, dtype *weights, dtype *biases, size_t m, size_t n, size_t k){
//     size_t row = blockIdx.y * blockDim.y + threadIdx.y; 
//     size_t col = blockIdx.x * blockDim.x + threadIdx.x;
//     size_t sum = 0;
//     if( col < k && row < m){
//         for(size_t i = 0; i < n; i++)
//             sum += A[row * n + i] * weights[i * k + col];
//         dst[row * k + col] = sum + biases[row];
//     }
// }

struct Matrix {
    const size_t w, h;
    dtype *vals;
    Matrix() : w{0}, h{0} {
        printf("err matrix\n");
        vals = nullptr;
    }
    Matrix(size_t w, size_t h) : w{w}, h{h} {
        cudaError_t e = cudaMallocManaged(&vals, w*h*sizeof(*vals));
        if(e)
            printf("alloc err: %i\n", e);
        else
            printf("alloced (%li, %li)\n", w, h);
    }
    ~Matrix(){
        if(vals)
            printf("freed (%li, %li)\n", w, h), cudaFree(vals);
        else
            printf("cant free err matrix");
    }

    dtype &at(size_t x, size_t y){ return *(vals + x + y*w); }

    template<typename FUNC_T, typename... ARGS_T>
    void cuda_call(size_t w, size_t h, FUNC_T func, ARGS_T... args){
        constexpr size_t BLOCK_SIZE = 16;
        size_t  grid_rows = (h + BLOCK_SIZE - 1) / BLOCK_SIZE;
        size_t  grid_cols = (w + BLOCK_SIZE - 1) / BLOCK_SIZE;
        dim3 dimGrid(grid_cols, grid_rows);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

        func<<<dimGrid,dimBlock>>>(args...);
    }

    void mul(Matrix &other, Matrix &result){
        if(w != other.h){
            printf("Cannot multiply w/ shapes A:(%li,%li) * B:(%li,%li)\n", w, h, other.w, other.h);
            return;
        }
        if(result.w != h || result.h != other.w){
            printf("Cannot store multiplication w/ shapes A:(%li,%li) * B:(%li,%li) into dst:(%li,%li)\n", w, h, other.w, other.h, result.w, result.h);
            return;
        }

        cuda_call(other.w, h, matrix_mul, result.vals, vals, other.vals, h, w, other.w);
    }

    void add(Matrix &other, Matrix &result){
        if(w != other.w || h != other.h){
            printf("Cannot add w/ shapes A:(%li,%li) * B:(%li,%li)\n", w, h, other.w, other.h);
            return;
        }
        if(result.w != w || result.h != h){
            printf("Cannot store addition w/ shapes A:(%li,%li) * B:(%li,%li) into dst:(%li, %li)\n", w, h, other.w, other.h, result.w, result.h);
        }

        cuda_call(w, h, matrix_add, result.vals, vals, other.vals, w);
    }

    void sub(Matrix &other, Matrix &result){
        if(w != other.w || h != other.h){
            printf("Cannot sub w/ shapes A:(%li,%li) * B:(%li,%li)\n", w, h, other.w, other.h);
            return;
        }
        if(result.w != w || result.h != h){
            printf("Cannot store subtraction w/ shapes A:(%li,%li) * B:(%li,%li) into dst:(%li, %li)\n", w, h, other.w, other.h, result.w, result.h);
        }

        cuda_call(w, h, matrix_sub, result.vals, vals, other.vals, w);
    }

    void zero_init(){
        cuda_call(w, h, matrix_zero, vals, w, h);
    }

    void xavier_init(){
        // Calculate standard deviation
        dtype xavier_standard_deviation = sqrt(2.0 / (w + h));
        std::default_random_engine random_generator(std::chrono::system_clock::now().time_since_epoch().count());

        // Create a normal distribution with mean 0 and Xavier standard deviation
        std::normal_distribution<double> distribution(0.0, xavier_standard_deviation);

        // Initialize the weights with random values from the distribution
        for (int i = 0; i < w*h; ++i)
                vals[i] = distribution(random_generator);
    }

    void transpose_vec(){
        if(w != 1 && h != 1){
            printf("can only transpose vec");
            return;
        }
        //swap
        *(size_t *)&h = h+w;
        *(size_t *)&w = h-w; // w <- h+w-w = h
        *(size_t *)&h -= w; // h+w-h = w
    }

    void print(){
        for(size_t y = 0; y < h; ++y){
            printf("[  ");
            for(size_t x = 0; x < w; ++x)
                printf("%f  ", at(x,y));
            printf("]\n");
        }
        printf("\n");
    }
};

template <size_t... LAYERS>
struct NeuralNetwork {
    static constexpr size_t num_layers = sizeof...(LAYERS);
    const size_t layer_dims[num_layers] = {LAYERS...};
    std::vector<Matrix> weights, biases,
        pre_activation_vals, activated_vals, gradient_vals, // values stored in network; before activation, after activation, and for gradiants
        dloss_wrt_weights, dloss_wrt_biases; // derivatives of loss wrt weights/biases

    NeuralNetwork(Matrix *train_x, Matrix *train_y, size_t batch_size){
        for(std::vector<Matrix> *v : {&weights, &biases, &pre_activation_vals, &activated_vals, &gradient_vals, &dloss_wrt_biases, &dloss_wrt_weights})
            v->reserve(num_layers-1);
        for(size_t i = 0; i < num_layers-1; ++i){
            weights.emplace_back(layer_dims[i],layer_dims[i+1]);
            weights.at(i).xavier_init();
            biases.emplace_back(1, layer_dims[i+1]);
            biases.at(i).zero_init();
        }
        for(size_t i = 0; i < num_layers; ++i)
            // layers.emplace_back(1, layer_dims[i]);
        cudaDeviceSynchronize();
    }

    void compute_outputs_and_gradients(){
        
    }
    
    void train(){

    }

    void print(){
        for(size_t i = 0; i < num_layers-1; ++i){
            printf("Weights:\n");
            weights.at(i).print();
            printf("Biases:\n");
            biases.at(i).print();
            printf("---\n");
        }
    }
};

int main(){
    // Matrix A(2,3);
    // Matrix B(3,2);

    // A.at(0,0) = 1;
    // A.at(1,0) = 2;
    // A.at(0,1) = 3;
    // A.at(1,1) = 4;
    // A.at(0,2) = 5;
    // A.at(1,2) = 1;

    // B.at(0,0) = 1;
    // B.at(1,0) = 2;
    // B.at(2,0) = 3;
    // B.at(0,1) = 3;
    // B.at(1,1) = 4;
    // B.at(2,1) = 5;

    // A.print();
    // B.print();

    // Matrix C = A.mul(B);
    // // Matrix C{2,2};
    // // test<<<1,1>>>(C);
    // cudaDeviceSynchronize();

    // C.print();

    Matrix w(10,4);
    Matrix v(1,10);
    Matrix res(4,1);

    for(size_t x = 0; x < w.w; ++x)
        for(size_t y = 0; y < w.h; ++y)
            w.at(x,y) = (x+y+1)/11.;

    for(size_t x = 0; x < v.w; ++x)
        for(size_t y = 0; y < v.h; ++y)
            v.at(x,y) = (x+y+1)/11.;

    w.mul(v,res);

    cudaDeviceSynchronize();

    w.print();
    v.print();
    res.print();
    res.transpose_vec();
    res.print();

    // NeuralNetwork<2,2> nn;
    
    // nn.print();
}

/*
__global__ void matrix_mul(Matrix *into, Matrix *from){
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if (row < n && col < n) {
        int value = 0;
        for (int i = 0; i < n; ++i) {
            value += A[i * n + row] * B[col * n + i];
        }
        into[col * n + row] = value;
    }
}

/////

__global__ void matrixMultiplication(int* A, int* B, int* C, int n) {
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    if (row < n && col < n) {
        int value = 0;
        for (int i = 0; i < n; ++i) {
            value += A[i * n + row] * B[col * n + i];
        }
        C[col * n + row] = value;
    }
}

int main() {
    constexpr int n = 3; // Size of square matrices

    // Input matrices
    int A[n * n] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int B[n * n] = {10, 11, 12, 13, 14, 15, 16, 17, 18};

    // Output matrix
    int C[n * n] = {0};

    // Allocate device memory
    int* d_A, * d_B, * d_C;
    cudaMalloc((void**)&d_A, n * n * sizeof(int));
    cudaMalloc((void**)&d_B, n * n * sizeof(int));
    cudaMalloc((void**)&d_C, n * n * sizeof(int));

    // Copy input matrices from host to device
    cudaMemcpy(d_A, A, n * n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, B, n * n * sizeof(int), cudaMemcpyHostToDevice);

    // Launch kernel
    dim3 threadsPerBlock(n, n);
    dim3 numBlocks(1, 1);
    if (n * n > 512) {
        threadsPerBlock.x = 512;
        threadsPerBlock.y = 512;
        numBlocks.x = ceil(double(n) / threadsPerBlock.x);
        numBlocks.y = ceil(double(n) / threadsPerBlock.y);
    }
    matrixMultiplication<<<numBlocks, threadsPerBlock>>>(d_A, d_B, d_C, n);

    // Copy the result matrix from device to host
    cudaMemcpy(C, d_C, n * n * sizeof(int), cudaMemcpyDeviceToHost);

    // Free device memory
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    // // Print the result
    // std::cout << "Result matrix:" << std::endl;
    // for (int i = 0; i < n; ++i) {
    //     for (int j = 0; j < n; ++j) {
    //         std::cout << C[j * n + i] << " ";
    //     }
    //     std::cout << std::endl;
    // }

    return 0;
}*/